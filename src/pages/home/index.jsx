import React, { useEffect, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import styles from './index.module.scss';
import { getMovies } from '../../graphql/film';
import CardSlider from '../../components/UI/slider/CardSlider';
import withSub from '../../HOC/withSub';
import { toast, ToastContainer } from 'react-toastify';
import { getCategories } from '../../graphql/queries/category';


const Index = () => {
    const [hover, setHover] = useState(false);
    const { loading: loadingC, error: errorC, data: dataC } = useQuery(getCategories);
    const { loading, error, data } = useQuery(getMovies)
    if (loading) {
        return loading
    }
    if (data) {

    }
    const handleMouseEnter = () => {
        window.scrollTo(0,0);
        setHover(true);
        console.log(hover)
    }

    return (
        <div>
            <div className={styles.main__container}>
                <div className={styles.bg__image__container}>
                    <img className={styles.bg__image} src={data.getMovies[0].image}></img>
                </div>
                <div className={styles.hero}></div>
                <ToastContainer />

                <>
                    {!loadingC ?
                        dataC.getCategories.map((category) => {
                            return (
                                <>
                                    {loading ? <h1> Loading</h1> :
                                        <CardSlider handleClick={handleMouseEnter} key={category.id} category={category.id} error={() => { errorToast() }} success={() => { successToast() }} title={category.name} data={data.getMovies}></CardSlider>
                                    }
                                </>
                            )
                        })
                        : <h1>loading</h1>
                    }

                </>

            </div>
        </div>
    );
};

export default withSub(Index);