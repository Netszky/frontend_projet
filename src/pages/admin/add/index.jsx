import React, { useState } from 'react';
import styles from './index.module.scss';
import withAdmin from '../../../HOC/withAdmin';
import { motion } from 'framer-motion';
import Form from '../../../components/UI/form/Form';
import { getCategories } from '../../../graphql/queries/category';
import { useQuery } from '@apollo/client';
import { getPersons } from '../../../graphql/queries/persons';
import { ToastContainer } from 'react-toastify';

const Index = (props) => {
    const [selected, setSelected] = useState(1);
    const { loading, error, data, refetch } = useQuery(getCategories, {
        context: {
            headers: {
                authorization: localStorage.getItem("token"),
            }
        }
    }
    );
    if (loading) {
        return "loading..."
    };
    if (error) {
        return null;
    };
    

    const optionsCat = [];
    data.getCategories.map((item) => {
        optionsCat.push({
            value: item.id,
            label: item.name
        })
    })


    return (
        <div className={styles.main__container}>
            <ToastContainer></ToastContainer>
            <div className={styles.label__container}>
                <ul className={styles.label}>
                    <li className={selected === 1 ? `${styles.label__text} ${styles.active}` : styles.label__text} onClick={() => { setSelected(1), refetch() }}>Categories</li>
                    <li className={selected === 4 ? `${styles.label__text} ${styles.active}` : styles.label__text} onClick={() => { setSelected(4), refetch() }}>Personnes</li>
                    <li className={selected === 2 ? `${styles.label__text} ${styles.active}` : styles.label__text} onClick={() => { setSelected(2), refetch() }}>Films</li>
                    {/* <li className={selected === 3 ? `${styles.label__text} ${styles.active}` : styles.label__text} onClick={() => { setSelected(3), refetch() }}>Series</li> */}
                </ul>
            </div>
            <div className={styles.metadata__container}>
                <Form optionsCat={optionsCat} selected={selected}></Form>
            </div>
            <div className={styles.metadata__container}>

            </div>
            <div className={styles.metadata__container}>

            </div>
        </div>
    );
};

export default withAdmin(Index);