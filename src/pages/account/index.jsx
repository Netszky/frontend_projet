import React, { useState } from 'react';
import styles from './profil.module.scss';
import { useMutation, useQuery } from '@apollo/client';
import { getUser, updateUser } from '../../graphql/queries/users';
import stripeService from "../../services/stripe.service";
import { useEffect } from 'react';
import Link from 'next/link';
import { cancelSub } from '../../utils/utils';
import { ToastContainer } from 'react-toastify';
const Index = () => {
    const [id, setID] = useState();
    const [updateUse] = useMutation(updateUser);
    const { data, loading, error, refetch } = useQuery(getUser, {
        variables: {
            id: id
        }
    })

    useEffect(() => {
        setID(JSON.parse(localStorage.getItem("user"))._id)
    }, [])

    const toDate = (date) => {
        const dat = new Date(date).getDate();
        const date1 = new Date(date).getMonth() + 1;
        const date2 = new Date(date).getFullYear();
        return dat + "/" + date1.toString() + "/" + date2.toString();
    }
    const body = {
        id: data && data.getUser.stripeID,
        user: data && data.getUser.id,
    }
    console.log(data);
    const handleCancel =  () => {
        const token = localStorage.getItem("token");
        const response =  stripeService.cancelSub(token, body).then((data)=>{
            updateUse({
                variables: {
                    id: id,
                    isSub: false
                },onCompleted: (data) => {
                    cancelSub();
                    console.log(data),
                    refetch();
                }
            })
        })
    }
    return (
        <div className={styles.account__container}>
            <ToastContainer/>
            {loading ? <h1>loading</h1> :
                <div className={styles.metadata__container}>
                    <h1>Bonjour {data.getUser.firstname}</h1>
                    <div className={styles.subscription}>
                        <h2>Votre Abonnement : </h2>
                        <h3>{data.getUser.sub.type === "price_1KRbJPJslrGuoRGT8Rpn9aLZ" ? "Abonnement Standard" : data.getUser.sub.type === "price_1KMrpJJslrGuoRGTy4EUxusI" ? "Abonnement Essentiel" : "Abonnement Premium"}</h3>
                        <p>Prix: {data.getUser.sub.price}€</p>
                        <p>Date de commencement: {toDate(parseInt(data.getUser.sub.create_date))}</p>
                        <p>Abonnement Actif : {data.getUser.isSub ? "Oui" : "Non"}</p>
                        <div className={styles.choose}>
                            <Link href={"/checkout"}>
                                <button>{"Changer d'Abonnement"}</button>

                            </Link>
                            <button onClick={() => {handleCancel()}}>Annuler Abonnement</button>
                        </div>
                    </div>
                </div>
            }
        </div>
    );
};

export default Index;