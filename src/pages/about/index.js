import React from 'react';
import TitlePage from '../../components/UI/title/TitlePage';

const index = () => {
    return (
        <div>
            <TitlePage title="About Page"/>
        </div>
    );
};

export default index;