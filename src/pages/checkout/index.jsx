import React, { useContext, useEffect, useState } from 'react';
import { loadStripe } from '@stripe/stripe-js';
import stripeService from "../../services/stripe.service";
import styles from "./checkout.module.scss";
import UserContext from '../../context/UserContext';

const stripePromise = loadStripe("pk_test_51KHUEnJslrGuoRGTo12dGxQQNkv7aZ8rDuTNZVpEfA8ADkafRIY67rg7rf7PJZB1wjMns5HeVbvHK0bMejYPWmPh00ONTA7VeO");

const Index = () => {
    const [selected, setSelected] = useState(1);
    const [priceId, setPriceId] = useState("price_1KMrpJJslrGuoRGTy4EUxusI")
    const { user } = useContext(UserContext);
   
    const body = {
        priceId: priceId,
        email: user && user.email
    }
    const handleConfirmation = async () => {
        const token = localStorage.getItem("token");
        try {
            const stripe = await stripePromise;
            const response = await stripeService.createSession(token, body);
            console.log(response)
            await stripe.redirectToCheckout({
                sessionId: response.id,
            });
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className={styles.checkout__container}>
            <div className={styles.title}>
                <div className={styles.checkout__header}>
                    <h1>Selectionnez le forfait qui vous convient.</h1>
                </div>
                <div>
                    <ul>
                        <li className={styles.checkmark__group}>
                            <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className={styles.checkmark} aria-hidden="true"><path fillRule="evenodd" clipRule="evenodd" d="M8.68239 19.7312L23.6824 5.73115L22.3178 4.26904L8.02404 17.6098L2.70718 12.293L1.29297 13.7072L7.29297 19.7072C7.67401 20.0882 8.28845 20.0988 8.68239 19.7312Z" fill="currentColor"></path></svg>
                            <p>Regardez autant que vous voulez. Sans publicité.</p>
                        </li>
                        <li className={styles.checkmark__group}>
                            <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className={styles.checkmark} aria-hidden="true"><path fillRule="evenodd" clipRule="evenodd" d="M8.68239 19.7312L23.6824 5.73115L22.3178 4.26904L8.02404 17.6098L2.70718 12.293L1.29297 13.7072L7.29297 19.7072C7.67401 20.0882 8.28845 20.0988 8.68239 19.7312Z" fill="currentColor"></path></svg>
                            <p>Recommandations personnalisées.</p>

                        </li>
                        <li className={styles.checkmark__group}>
                            <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className={styles.checkmark} aria-hidden="true"><path fillRule="evenodd" clipRule="evenodd" d="M8.68239 19.7312L23.6824 5.73115L22.3178 4.26904L8.02404 17.6098L2.70718 12.293L1.29297 13.7072L7.29297 19.7072C7.67401 20.0882 8.28845 20.0988 8.68239 19.7312Z" fill="currentColor"></path></svg>
                            <p>Changez ou annulez votre forfait à tout moment.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div className={styles.plangrid}>
                <div className={styles.plangrid__header}>
                    <div className={styles.plangrid__selector}>
                        <span onClick={() => {setSelected(1), setPriceId({...priceId, id:"price_1KMrpJJslrGuoRGTy4EUxusI"})}} className={selected == 1 ?`${styles.plangrid__label} ${styles.active}` : styles.plangrid__label}>Essentiel</span>
                        <span onClick={() => {setSelected(2), setPriceId({...priceId, id:"price_1KRbJPJslrGuoRGT8Rpn9aLZ"})}} className={selected == 2 ?`${styles.plangrid__label} ${styles.active}` : styles.plangrid__label}>Standard</span>
                        <span onClick={() => {setSelected(3), setPriceId({...priceId, id:"price_1KRbdBJslrGuoRGTKP7PnxNC"})}} className={selected == 3 ?`${styles.plangrid__label} ${styles.active}` : styles.plangrid__label}>Premium</span>
                    </div>
                </div>
                <table className={styles.plangrid__table}>
                    <tbody>
                        <tr className={styles.row}>
                            <td className={styles.cell__head} >Abonnement Mensuel</td>
                            <td className={ selected == 1 ? `${styles.cell} ${styles.checkmark}` : styles.cell} >9,99</td>
                            <td className={ selected == 2 ? `${styles.cell} ${styles.checkmark}` : styles.cell}>13,49</td>
                            <td className={ selected == 3 ? `${styles.cell} ${styles.checkmark}` : styles.cell}>17,99</td>
                        </tr>
                        <tr className={styles.row}>
                            <td className={styles.cell__head}>Qualité vidéo</td>
                            <td className={ selected == 1 ? `${styles.cell} ${styles.checkmark}` : styles.cell}>Bonne</td>
                            <td className={ selected == 2 ? `${styles.cell} ${styles.checkmark}` : styles.cell}>Meilleure</td>
                            <td className={ selected == 3 ? `${styles.cell} ${styles.checkmark}` : styles.cell}>Optimale</td>
                        </tr>
                        <tr className={styles.row}>
                            <td className={styles.cell__head}>Résolution</td>
                            <td className={ selected == 1 ? `${styles.cell} ${styles.checkmark}` : styles.cell}>480p</td>
                            <td className={ selected == 2 ? `${styles.cell} ${styles.checkmark}` : styles.cell}>1080p</td>
                            <td className={ selected == 3 ? `${styles.cell} ${styles.checkmark}` : styles.cell}>4K+HDR</td>
                        </tr>
                        <tr className={styles.row}>
                            <td className={styles.cell__head}>Netflix sur votre TV, ordinateur, smartphone et tablette</td>
                            <td className={styles.cell}><svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className={selected == 1 && styles.checkmark} aria-hidden="true"><path fillRule="evenodd" clipRule="evenodd" d="M8.68239 19.7312L23.6824 5.73115L22.3178 4.26904L8.02404 17.6098L2.70718 12.293L1.29297 13.7072L7.29297 19.7072C7.67401 20.0882 8.28845 20.0988 8.68239 19.7312Z" fill="currentColor"></path></svg></td>
                            <td className={styles.cell}><svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className={selected == 2 && styles.checkmark} aria-hidden="true"><path fillRule="evenodd" clipRule="evenodd" d="M8.68239 19.7312L23.6824 5.73115L22.3178 4.26904L8.02404 17.6098L2.70718 12.293L1.29297 13.7072L7.29297 19.7072C7.67401 20.0882 8.28845 20.0988 8.68239 19.7312Z" fill="currentColor"></path></svg></td>
                            <td className={styles.cell}><svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className={selected == 3 && styles.checkmark} aria-hidden="true"><path fillRule="evenodd" clipRule="evenodd" d="M8.68239 19.7312L23.6824 5.73115L22.3178 4.26904L8.02404 17.6098L2.70718 12.293L1.29297 13.7072L7.29297 19.7072C7.67401 20.0882 8.28845 20.0988 8.68239 19.7312Z" fill="currentColor"></path></svg></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className={styles.btn__container}>
                <button className={styles.btn__primary} onClick={handleConfirmation}>Payer</button>
            </div>

        </div>
    );
};

export default Index;