import React, { useEffect, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import styles from './index.module.scss';
import { getMovieByCategory, getMovies } from '../../graphql/film';
import CardSlider from '../../components/UI/slider/CardSlider';
import withSub from '../../HOC/withSub';
import { toast, ToastContainer } from 'react-toastify';
import { getCategories } from '../../graphql/queries/category';
import Select from 'react-select';


const Index = () => {
    const [hover, setHover] = useState(false);
    const [name, setName] = useState();
    const [category, setCategory] = useState();
    const [movie, setMovie] = useState();
    const { loading: loadingC, error: errorC, data: dataC } = useQuery(getCategories);
    const { loading, error, data, refetch } = useQuery(getMovies)
    
    if (loading) {
        return loading
    }
    const optionsCat = [];
    if (data) {
        dataC.getCategories.map((item) => {
            optionsCat.push({
                value: item.id,
                label: item.name
            })
        })
    }
    const handleMouseEnter = () => {
        setHover(true);
        console.log(hover)
    }
    const handleChangeCategory = (selectedOption) => {
        setCategory(selectedOption.label);
        console.log(selectedOption.label)
        setName(selectedOption.label);
        const result = data.getMovies.filter((item) => item.categories === category)
        console.log(data.getMovies)
        console.log(result);
    }
    
    
    return (
        <div>
            <div className={styles.main__container}>
                <div className={styles.bg__image__container}>
                    <Select onChange={handleChangeCategory} placeholder="Categories" className={styles.select} options={optionsCat}></Select>
                </div>
                <div className={styles.hero}></div>
                <ToastContainer />


                {loading ? <h1> Loading</h1> :
                    <CardSlider handleClick={handleMouseEnter} error={() => { errorToast() }} success={() => { successToast() }} title={name} data={movie}></CardSlider>
                }




            </div>
        </div>
    );
};

export default withSub(Index);