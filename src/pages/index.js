import Head from 'next/head'
import { useState } from 'react';
import HomeModal from '../components/UI/HomeModal/HomeModal';

import styles from "./index.module.scss";

export default function Home() {
  const [modal, setModal] = useState(true);
  const hoverHandler = () => {
    setModal(false);
  }
  return (
    <>
      <Head>
        <title>Netflix</title>
        <link rel="icon" href="https://assets.nflxext.com/us/ffe/siteui/common/icons/nficon2016.png"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
      </Head>
      <main>
        {
          modal &&
          <HomeModal onClick={hoverHandler}></HomeModal>
        }
        <div className={styles.bg}>
          
          <img height="100%" width="100%" className={styles.bg__image} src="https://assets.nflxext.com/ffe/siteui/vlv3/8f3e38a9-1c2c-4818-99b5-2d9bdff9ad1d/6c1bedbe-f9c7-4de5-ae33-f4950ce13fff/FR-fr-20220124-popsignuptwoweeks-perspective_alpha_website_small.jpg" alt="" />
        </div>
        <div className={styles.main__container}>
          <h1>Films, séries TV et bien plus en illimité.</h1>
          <h2>Où que vous soyez. Annulez à tout moment.</h2>
        </div>
      </main>
    </>
  )
}
