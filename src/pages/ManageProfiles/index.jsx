import { useMutation, useQuery } from '@apollo/client';
import Link from 'next/link';
import React, { useContext, useEffect, useState } from 'react';
import ProfilCard from '../../components/UI/profilcard/profilcard';
import ProfilCardList from '../../components/UI/ProfilCardList/ProfilCardList';
import UserContext from '../../context/UserContext';
import { getProfilsImages } from '../../graphql/queries/image';
import { createProfil, deleteProfil, updateProfil } from '../../graphql/queries/profil';
import { getUserProfiles, updateUserProfil } from '../../graphql/queries/users';
import styles from "../browse/index.module.scss";
import styles2 from "./index.module.scss";


const Index = () => {
    const [updateapolloclient] = useMutation(updateProfil);
    const [createProfilApollo] = useMutation(createProfil);
    const [updateUserProfils] = useMutation(updateUserProfil);
    const [deleteprofil] = useMutation(deleteProfil);
    const [changeProfil, setChangeProfil] = useState(false);
    const [newProfil, setNewProfil] = useState(false);
    const [profil, setProfil] = useState();
    const [name, setName] = useState();
    const [profilImage, setProfilImage] = useState("https://res.cloudinary.com/dh7pvolby/image/upload/v1644396412/ablette_em8tj9.png");
    const { token } = useContext(UserContext);
    const [user, setUser] = useState();

    useEffect(() => {
        setUser(JSON.parse(localStorage.getItem("user")))
    }, [])
    const handler = (image) => {
        setProfilImage(image)
    }
    const { loading, error, data, refetch } = useQuery(getUserProfiles, {
        context: {
            headers: {
                authorization: token,
            }
        }
    }
    )
    if (loading) {
        return "loading..."

    }
    if (error) {
        console.log(error);
        return null;
    };

    const chooseProfil = (e, profil) => {
        e.preventDefault();
        setProfil(profil);
        setChangeProfil(true);
        setProfilImage(profil.image)
    }

    const submitHandlerCreateProfil = (e, name, image) => {
        e.preventDefault();
        createProfilApollo({
            variables: {
                name: name,
                image: image
            },
            onCompleted: (data) => {
                updateUserProfils({
                    variables: {
                        id: user._id,
                        profil: data.createProfil.id
                    },
                    onCompleted: (data) => {
                        refetch();
                    }
                })
                refetch();
                setNewProfil(false);
            },
            onError: (error) => {

            }
        })

    }
    const sublitHandlerDeleteProfil = (e, profil) => {
        e.preventDefault();
        deleteprofil({
            variables: {
                id: profil.id
            },
            onCompleted: (data) => {
                refetch();
            }
        })
        setChangeProfil(false);
    }

    const submitHandlerProfil = (e, name, image, id) => {
        e.preventDefault();
        const token = localStorage.getItem("token");
        updateapolloclient({
            context: {
                headers: {
                    authorization: token,
                },
            },
            variables: {
                id: id,
                name: name,
                image: image
            },
            onCompleted: (data) => {
                refetch();
            },
            onError: (data) => {

            },


        })
        setChangeProfil(false);
    }
    return (
        <div className={styles.profil__container}>
            <div className={styles.profil__list}>
                <h1 className={styles.label}>Gestion des profils</h1>
                <ul className={styles.choose__profil}>
                    {data.getUserProfil.profil.map((profil) => {
                        return (
                            <Link href="/home" key={profil.id}>
                                <li className={styles.profil} onClick={(e) => (chooseProfil(e, profil))}>
                                    <div>
                                        <div className={styles.test}>
                                            <svg className={styles.svg__white} viewBox="0 0 24 24">
                                                <path d="M22.2071 7.79285L15.2071 0.792847L13.7929 2.20706L20.7929 9.20706L22.2071 7.79285ZM13.2071 3.79285C12.8166 3.40232 12.1834 3.40232 11.7929 3.79285L2.29289 13.2928C2.10536 13.4804 2 13.7347 2 14V20C2 20.5522 2.44772 21 3 21H9C9.26522 21 9.51957 20.8946 9.70711 20.7071L19.2071 11.2071C19.5976 10.8165 19.5976 10.1834 19.2071 9.79285L13.2071 3.79285ZM17.0858 10.5L8.58579 19H4V14.4142L12.5 5.91417L17.0858 10.5Z" />
                                            </svg>
                                        </div>
                                        <div className={styles.profil__icon}>
                                            <img alt='profilimage' className={styles.profil__image} src={profil.image}></img>
                                        </div>
                                        <span className={styles.profil__name}>{profil.name}</span>
                                    </div>
                                </li>
                            </Link>
                        )
                    })}
                    <li className={styles.profil} onClick={() => setNewProfil(true)}>
                        <div>
                            <div className={styles.profil__add}>
                                <svg className={styles.svg__white} viewBox="0 0 24 24">
                                    <path d="M17,13H13V17H11V13H7V11H11V7H13V11H17M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                                </svg>
                            </div>
                            <span className={styles.profil__name}>Ajouter un profil</span>
                        </div>
                    </li>

                </ul>
                <Link href="/browse">
                    <button className={styles.profil__button}>Terminer</button>
                </Link>
            </div>
            {
                changeProfil &&
                <div className={styles2.edit__profil}>
                    <div className={styles2.edit__profil__action}>

                        <h1>Modifier le profil</h1>
                        <div className={styles2.profil__metadata}>
                            <div className={styles2.metadata__image__container}>
                                <img alt='profilimage' className={styles2.metadata__image} width="70px" height="70px" src={profilImage}></img>
                            </div>
                            <input type="text" defaultValue={profil.name} onChange={(e) => { setName(e.target.value) }} className={styles2.profil__input}></input>
                            <input type="text" defaultValue={profil.image} value={profilImage} className={styles2.profil__input} readOnly></input>
                        </div>
                        <button className={styles2.profil__button} onClick={(e) => { submitHandlerProfil(e, name, profilImage, profil.id) }}>Enregistrer</button>
                        <button className={styles2.profil__button} onClick={() => { setChangeProfil(false) }}>Annuler</button>
                        <button className={styles2.profil__button} onClick={(e) => { sublitHandlerDeleteProfil(e, profil) }}>Supprimer le profil</button>
                    </div>
                    <div className={styles2.gallery__image}>
                        <ProfilCardList handler={handler}></ProfilCardList>
                    </div>
                </div>
            }
            {
                newProfil &&
                <div className={styles2.edit__profil}>
                    <div className={styles2.edit__profil__action}>
                        <h1>Nouveau profil</h1>
                        <div className={styles2.profil__metadata}>
                            <div className={styles2.metadata__image__container}>
                                <ProfilCard url={profilImage}></ProfilCard>
                            </div>
                            <input type="text" required="true" placeholder='Name' onChange={(e) => { setName(e.target.value) }} className={styles2.profil__input}></input>
                            <input type="text" required="true" placeholder='Image (URL)' readOnly className={styles2.profil__input} value={profilImage}></input>
                        </div>
                        <button className={styles2.profil__button} onClick={(e) => { submitHandlerCreateProfil(e, name, profilImage) }}>Enregistrer</button>
                        <button className={styles2.profil__button} onClick={() => { setNewProfil(false) }}>Annuler</button>
                    </div>
                    <div className={styles2.gallery__image}>
                        <ProfilCardList handler={handler}></ProfilCardList>
                    </div>
                </div>
            }

        </div>
    )
};

export default Index;