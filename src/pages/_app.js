import MainLayout from '../components/layouts/MainLayout'
import '../styles/globals.scss'
import { ApolloProvider, mergeOptions } from '@apollo/client';
import client from "../apollo/apollo-client";
import { useRouter } from 'next/router';
import LoginLayout from '../components/layouts/LoginLayout';
import { motion } from "framer-motion";
import { UserContextProvider } from '../context/UserContext';
import 'react-toastify/dist/ReactToastify.css';
import Head from 'next/head';

function MyApp({ Component, pageProps, router }) {
  const { asPath } = useRouter();
  return (
    <ApolloProvider client={client}>
      <Head>
        <title>Netflix</title>
        <link rel="icon" href="https://assets.nflxext.com/us/ffe/siteui/common/icons/nficon2016.png"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
      </Head>
      <UserContextProvider>
      <motion.div
        key={router.route}
        initial="initial"
        animate="animate"
        transition={{delay: 0.1, duration: 0.3}}
        variants={{
          initial: {
            x: 50
          },
          animate: {
            x: 0
          }
        }}>
      {asPath === "/login" ? <LoginLayout ><Component {...pageProps} /></LoginLayout> 
      : asPath === "/register" ? <LoginLayout ><Component {...pageProps} /></LoginLayout> : asPath === "/checkout" ? <LoginLayout><Component {...pageProps} /></LoginLayout>
      : <MainLayout>
        <Component {...pageProps} />
      </MainLayout>
      }
      </motion.div>
      </UserContextProvider>
    </ApolloProvider>
  )

}

export default MyApp
