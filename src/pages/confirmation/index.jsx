import React, { useContext, useEffect } from 'react';
import { useRouter } from 'next/router';
import styles from './index.module.scss';
import Link from 'next/link';
const Index = () => {
    const router = useRouter();

    return (
        <div className={styles.confirmation__container}>
            <h1>Votre abonnement à bien été pris en compte !</h1>
            <p>Vous recevrez un email de confirmation à l&aposadresse de votre compte !</p>
            <p>L&aposequipe vous remercie pour votre achat</p>
            <Link href="/browse">Accueil</Link>

        </div>
    );
};

export default Index;