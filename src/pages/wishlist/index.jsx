import React from 'react';
import styles from './index.module.scss';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { getProfil, updateWishlist } from '../../graphql/queries/profil';
import { useEffect } from 'react';
import { useState } from 'react';
import CardList from '../../components/UI/CardList/CardList';
import { ToastContainer } from 'react-toastify';
import { errorToast } from '../../utils/utils';


const Index = () => {
    const [profil, setProfil] = useState([]);
    const [updateWish] = useMutation(updateWishlist);
    const [wishlist, setWishlist] = useState([]);
    const { error, loading, data, refetch } = useQuery(getProfil, {
        variables: {
            id: profil.id
        },
        onCompleted: (data) => {
                setWishlist(data.getProfil.wishlist)
        }
    });
    useEffect(() => {
        setProfil(JSON.parse(localStorage.getItem("profil")));
    }, [])

    const deleteFromWishlist = async (newWishlist, name) => {
        const wish = []
        data.getProfil.wishlist.map((movie) => [
            wish.push(movie.id)
        ])
        const indexofRemove = wish.indexOf(newWishlist)
        wish.splice(indexofRemove, 1);

        updateWish({
            variables: {
                id: profil.id,
                wishlist: wish
            }, onCompleted: (data) => {
                refetch();
                errorToast(name);
            }
        }
        )
    }
    return (
        <>
            <div className={styles.container}>
                <div className={styles.header__title}>
                    <h2>Ma Liste</h2>
                </div>
                <div className={styles.card__container}>
                    <ToastContainer />
                    {loading ? <h1> Loading </h1> :
                        wishlist.map((movie) => {
                            return (
                                <CardList key={movie.id} onClick={deleteFromWishlist} movie={movie}></CardList>
                            )
                        })
                    }
                </div>
            </div>
        </>
    );
};

export default Index;