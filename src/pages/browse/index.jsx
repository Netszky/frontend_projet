import React, { useContext, useEffect, useState } from 'react';
import styles from "./index.module.scss";
import { useQuery } from '@apollo/react-hooks';

import { getUserProfiles } from '../../graphql/queries/users';

import Link from 'next/link';
import UserContext from '../../context/UserContext';

const Index = () => {

    const { token } = useContext(UserContext);
    const [profilUser, setProfil] = useState();
    const [loadingD, setLoading] = useState(true);
    const { loading, error, data } = useQuery(getUserProfiles, {
        context: {
            headers: {
                authorization: token,
            }
        }, onCompleted: (data) =>{
            
            setLoading(false)
        }
    }
    )
    return (

        <div className={styles.profil__container}>
            <div className={styles.profil__list}>
                <h1 className={styles.label}>Qui est-ce ?</h1>
                <ul className={styles.choose__profil}>
                    {!loading ? 
                       data.getUserProfil.profil.map((profil) => {
                            return (
                                <Link href="/home" key={profil.id}>
                                    <li className={styles.profil} onClick={() => (localStorage.setItem("profil", JSON.stringify(profil)))}>
                                        <div>
                                            <div className={styles.profil__icon}>
                                                <img className={styles.profil__image} src={profil.image}></img>
                                            </div>
                                            <span className={styles.profil__name}>{profil.name}</span>
                                        </div>
                                    </li>
                                </Link>
                            )
                        })
                    : <h1>Loading</h1>}


                </ul>
                <Link href="/ManageProfiles">
                    <button className={styles.profil__button}>Gerer les profils</button>
                </Link>
            </div>
        </div>
    );
};

export default Index;