import { useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import React from 'react';
import ReactPlayer from 'react-player';
import { getMovie } from '../../graphql/film';
import styles from './index.module.scss';

const Index = () => {
    const router = useRouter();
    const { id } = router.query
    const { loading, error, data } = useQuery(getMovie, {
        variables: {
            id: id
        }
    })
    return (
        <>
            {!loading &&
                <div className={styles.container}>
                    {console.log(data)}
                    <h1 className={styles.movie__title}>{data.getMovie.name}</h1>
                    <div className={styles.player__container}>
                        <ReactPlayer controls={true} url={data.getMovie.url}></ReactPlayer>
                    </div>
                    <div className={styles.movie__data}>
                        <div className={styles.movie__description}>
                            <p>{data.getMovie.description}</p>
                        </div>
                        <div className={styles.info}>
                            <div className={styles.info__detail}>
                                <p>Categories :</p> {data.getMovie.categories.map((cat) => {
                                    return (
                                        <p key={cat.id}>{cat.name}</p>
                                    )
                                })}
                            </div>
                            <div className={styles.info__detail}>
                                <p>Acteurs: </p> {data.getMovie.actor.map((actor) => {
                                    return <p key={actor.id}>{actor.name}</p>
                                })}
                            </div>
                            <div className={styles.info__detail}>
                                <p>Réalisateur: </p> {data.getMovie.director.map((actor) => {
                                    return <p key={actor.id}>{actor.name}</p>
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
};

export default Index;