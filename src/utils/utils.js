import { toast, ToastContainer } from 'react-toastify';


export const successToast = (name) => {
   return toast.success(`Added ${name} `, {
      position: 'top-center',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
   });
}
export const infoToast = (name) => {
   return toast.info(`${name} Already Added `, {
      position: 'top-center',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
   });
}
export const errorToast = (name) => {
   return toast.error(`Deleted ${name} `, {
      position: 'top-center',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
   });
}
export const cancelSub = (name) => {
   return toast.success(`Subscription Canceled`, {
      position: 'top-center',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
   });
}
