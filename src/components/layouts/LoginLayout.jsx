import React from 'react';
import Sidebar from '../UI/sidebar/Sidebar';

const LoginLayout = ({ children }) => {
   
    const navItems = [
        {
            
            "svg": "M19.07,4.93C17.22,3 14.66,1.96 12,2C9.34,1.96 6.79,3 4.94,4.93C3,6.78 1.96,9.34 2,12C1.96,14.66 3,17.21 4.93,19.06C6.78,21 9.34,22.04 12,22C14.66,22.04 17.21,21 19.06,19.07C21,17.22 22.04,14.66 22,12C22.04,9.34 21,6.78 19.07,4.93M17,12V18H13.5V13H10.5V18H7V12H5L12,5L19.5,12H17Z",
            "name" : "Home",
            "link": "/"
        },
        {
            "svg": "M3,4H7V8H3V4M9,5V7H21V5H9M3,10H7V14H3V10M9,11V13H21V11H9M3,16H7V20H3V16M9,17V19H21V17H9",
            "name" : "Catégories",
            "link": "/categories"
        },
        {
            "svg": "M20,2H4A2,2 0 0,0 2,4V22L6,18H20A2,2 0 0,0 22,16V4C22,2.89 21.1,2 20,2Z",
            "name" : "Contact",
            "link": "/contact"
        }
    ]
    return (
        <>
            
            <Sidebar nav={navItems}></Sidebar>
            {children}
        </>
    );
};

export default LoginLayout;