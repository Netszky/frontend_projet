import React from 'react';
import Footer from '../UI/footer/Footer';
import Header from '../UI/header/Header';

const MainLayout = ({ children }) => {
    return (
        <>
        <Header></Header>   
            <main>
                {children}
            </main>
            {/* <Footer></Footer> */}
        </>
    );
};

export default MainLayout;