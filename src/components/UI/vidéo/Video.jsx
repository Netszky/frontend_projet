import React, { useState } from 'react';
import ReactPlayer from 'react-player';
import styles from './Video.module.scss';

const Video = (props) => {
    const [play, setPlay] = useState(false);
    const handleMouseEnter = () => {
        setPlay(true);
    };
    const handleMouseLeave = () => {
        setPlay(false);
    };


    return (
        <div className={styles.video__container} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
            {play &&
                <ReactPlayer
                    width='100%'
                    height='50%'
                    playing={play}
                    controls={false}
                    config={{ file: { forceHLS: true } }}
                    url={props.url}
                />
            }
        </div>
    );
};

export default Video;