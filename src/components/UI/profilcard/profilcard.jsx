import React from 'react';
import styles from './profilcard.module.scss';

const ProfilCard = (props) => {
    return (
        <div onClick={props.onClick} className={styles.profil__card}>
            <img className={styles.profil__image} src={props.url}></img>
        </div>
    );
};

export default ProfilCard;