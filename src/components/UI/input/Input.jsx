import React from 'react';

const Input = (props) => {
    return (
        <>
            {/* <label>{props.label}</label> */}
            <input className={props.class} value={props.value} required={props.required} type={props.type} placeholder={props.placeholder}  id={props.id} onChange={props.onChange}></input>
        </>
    );
};

export default Input;