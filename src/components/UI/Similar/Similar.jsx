import { useQuery } from '@apollo/client';
import React from 'react';
import { useState } from 'react';
import { getMovieByCategory } from '../../../graphql/film';
import Card from '../Card/Card';
import styles from './Similar.module.scss';

const Similar = (props) => {
    const [movieA, setMovie] = useState([]);
    const { loading, error, data } = useQuery(getMovieByCategory, {
        variables: {
            id: props.category
        }, onCompleted: (data) => {
            data.getMoviesByCategory.map((movie) => {
                if (movie.id === props.movie) {
                    console.log("bob")
                } else {
                    movieA.push(movie)
                }
            })
        }
    })

    return (
        <div className={styles.slider__container}>
            <div className={styles.movie__container}>
                {movieA.length > 0 && movieA.map((movie) => (
                    <>
                        <Card handleClick={props.handleClick} key={movie.id} movie={movie} ></Card>
                    </>
                ))}
            </div>
        </div>
    );
};

export default Similar;