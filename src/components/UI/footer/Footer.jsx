import Link from 'next/link';
import React from 'react';
import styles from './Footer.module.scss';

const Footer = () => {
    return (
        <footer className={styles.footer__container}>
            <div className="footer__menu">
                <Link href="/about">About</Link>
                <Link href="/contact">Contact</Link>
            </div>
        </footer>
    );
};

export default Footer;