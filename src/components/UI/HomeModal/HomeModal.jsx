import React from 'react';
import styles from './HomeModal.module.scss';

const HomeModal = (props) => {
    return (
        <div className={styles.modal__container}>
            <div className={styles.main__container}>
                <h1>A Lire Attentivement !</h1>
                <h2>Projet Netflix M1 DEV WEB</h2>
                <div className={styles.disclaimer}>
                    <h2>Ce qui a été fait</h2>
                    <ul>
                        <h3>Backend (Heroku)</h3>
                        <li>GraphQL (sauf Login / Register)</li>
                        <li>Webhooks Stripe</li>
                    </ul>
                    <ul>
                        <h3>FrontEnd (Vercel)</h3>
                        <li>Multi profil</li>
                        <li>Page Accueil films triés par catégories</li>
                        <li>Page Film unique</li>
                        <li>Login / Logout / Register</li>
                        <li>Envoie de mail : Register et Abonnement (Lib : Sendgrid template Dynamiques)</li>
                        <li>Crud Avec upload d&apos;image Cloudinary</li>
                        <li>WishList avec Toaster (Lib: Toastify)</li>
                        <li>SCSS</li>
                        <li>Abonnement et Desabonnement Stripe</li>
                        <li>Lecture vidéo (React Player)</li>
                    </ul>
                    <h2>Problèmes / A faire</h2>
                    <ul>
                        <li>Certaines pages peuvent ne pas se charger en raison des query graphQL (il faut refresh la page)</li>
                        <li>Responsive</li>
                        <li>Optimisation du CSS</li>
                        <li>Optimisation des composants</li>
                    </ul>
                    <div className={styles.accept__button}>
                        <button onClick={() => {props.onClick()}}>J&apos;ai Compris</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HomeModal;