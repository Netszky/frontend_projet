import React, { useState } from 'react';
import styles from './form.module.scss';
import Select from 'react-select'
import Axios from 'axios';
import { createCategory, getCategories } from '../../../graphql/queries/category';
import { useMutation, useQuery } from '@apollo/client';
import { createPerson, getPersons } from '../../../graphql/queries/persons';
import { createMovie } from '../../../graphql/film';
import { successToast } from '../../../utils/utils';

const Categories = (props) => {
    const [name, setName] = useState();
    const [createCategoryApollo] = useMutation(createCategory);
    const [addPerson] = useMutation(createPerson);
    const submitHandler = (e) => {
        e.preventDefault();
        if (props.selected === 1) {

            createCategoryApollo({
                context: {
                    headers: {
                        authorization: localStorage.getItem("token")
                    }
                },
                variables: {
                    name: name
                },
                onCompleted: (data) => {
                    successToast(data.createCategory.name);
                    console.log(data);
                }, onError: (err) => {
                    console.log(err);
                }
            })
        }
        if(props.selected === 4) {
            addPerson({
                context: {
                    headers: {
                        authorization: localStorage.getItem("token")
                    }
                },
                variables: {
                    name: name
                },
                onCompleted: (data) => {
                    successToast(data.createPerson.name);
                    props.fetch();

                }
            })
        }
    
    }
    return (
        <form onSubmit={(e) => { submitHandler(e) }}>
            <label>Name</label>
            <input onChange={(e) => { setName(e.target.value) }} required={true} type="text"></input>
            <button type="submit" className="button__primary">Ajouter</button>
        </form>
    )
}
const Series = (props) => {
    const [table, setTable] = useState([]);
    const [image, setImage] = useState();
    const [metadata, setMetadata] = useState({});
    const [createMovieApollo] = useMutation(createMovie);

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("file", image[0]);
        formData.append("upload_preset", "hdanxjax");

        Axios.post("https://api.cloudinary.com/v1_1/dh7pvolby/image/upload", formData)
            .then((res) => {
                createMovieApollo({
                    context: {
                        headers: {
                            authorization: localStorage.getItem("token")
                        }
                    },
                    variables: {
                        name: metadata.name,
                        actor: metadata.actor,
                        duration: metadata.duration,
                        director: metadata.director,
                        description: metadata.description,
                        url: metadata.url,
                        year: parseInt(metadata.year),
                        categories: metadata.categories,
                        image: res.data.secure_url
                    }, onCompleted: (data) => {
                        successToast(data.createMovie.name);
                        console.log(data)
                    }
                })
            })
            .catch((err) => console.log(err))

    }
    const handleChange = (selectedOption) => {
        const array = []
        selectedOption.map((item) => {
            array.push(item.value)
        })
        setMetadata({ ...metadata, director: array })
        console.log(`Option selected:`, selectedOption);
    }

    const handleChangeActor = (selectedOption) => {
        const array = []
        selectedOption.map((item) => {
            array.push(item.value)
        })
        setMetadata({ ...metadata, actor: array })
        console.log(`Option selected:`, selectedOption);
    }
    const handleChangeCategory = (selectedOption) => {
        const array = []
        selectedOption.map((item) => {
            array.push(item.value)
        })
        setMetadata({ ...metadata, categories: array })
        console.log(`Option selected:`, selectedOption);
    }
    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <label>Name</label>
            <input onChange={(e) => { setMetadata({ ...metadata, name: e.target.value }) }} required={true} type="text"></input>
            <label>Categories</label>
            <Select onChange={handleChangeCategory} className={styles.selectbox} isMulti={true} options={props.optionsCat}></Select>
            <label>Acteurs</label>
            <Select onChange={handleChangeActor} className={styles.selectbox} isMulti={true} options={props.optionsAct}></Select>
            <label>Duration</label>
            <input onChange={(e) => { setMetadata({ ...metadata, duration: e.target.value }) }} type="text"></input>
            <label>Director</label>
            <Select onChange={handleChange} className={styles.selectbox} isMulti={true} options={props.optionsAct}></Select>
            <label>Année</label>
            <input onChange={(e) => { setMetadata({ ...metadata, year: e.target.value }) }} type="number"></input>
            <label>Description</label>
            <textarea onChange={(e) => { setMetadata({ ...metadata, description: e.target.value }) }}></textarea>
            <label>Image</label>
            <input required={false} onChange={(e) => { setImage(e.target.files) }} type="file"></input>
            <label>Url Video</label>
            <input onChange={(e) => { setMetadata({ ...metadata, url: e.target.value }) }} type="text"></input>
            <button type='submit' className="button__primary">Ajouter</button>
        </form>
    )
}

const Form = (props) => {
    const { loading: loadingP, error: errorP, data: dataP, refetch } = useQuery(getPersons);
    if (loadingP) {
        return "loading..."
    };
    if (errorP) {
        return null;
    };
    const optionsAct = [];
    dataP.getPersons.map((item) => {
        optionsAct.push({
            value: item.id,
            label: item.name
        })
    })
    return (
        <div className={styles.metadata__container}>
            {props.selected === 2 ?
                <Series selected={props.selected} optionsAct={optionsAct} optionsCat={props.optionsCat}></Series>
                : props.selected === 3 ? <Series selected={props.selected} optionsCat={props.optionsCat}> optionsAct={props.optionsAct}</Series>
                    : props.selected === 1 ? <Categories selected={props.selected}></Categories> : props.selected === 4 ? <Categories fetch={refetch} selected={props.selected}></Categories>
                        : null
            }
        </div>
    );
};

export default Form;