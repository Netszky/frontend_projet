import React, { useState } from 'react';
import Card from '../Card/Card';
import styles from './Slider.module.scss';
import Link from 'next/link';
import { useQuery } from '@apollo/client';
import { getMovieByCategory } from '../../../graphql/film';
const CardSlider = (props) => {
    const [loadingMovie, setLoadingMovie] = useState(true);
    const { loading, error, data } = useQuery(getMovieByCategory, {
        variables: {
            id: props.category
        }, onCompleted: (data) => {
            if (data) {
                setLoadingMovie(false);
            }

        }
    })
    return (
        <>
            {!loadingMovie &&
                data.getMoviesByCategory.length  !== 0 ?
                    <div className={styles.row__container}>
                    <Link href={"/movie"}><h1 className={styles.label__row}>{props.title}</h1></Link>
                    <div className={styles.slider}>
                        {data.getMoviesByCategory.map((movie) => (
                            <>
                                <Card handleClick={props.handleClick} key={movie.id} movie={movie} ></Card>
                            </>
                        ))}
                    </div>
                </div>
             : null
            }
        </>
    );
};

export default CardSlider;