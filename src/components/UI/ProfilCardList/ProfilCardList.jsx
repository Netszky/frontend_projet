import { useQuery } from '@apollo/client';
import React from 'react';
import { getProfilsImages } from '../../../graphql/queries/image';
import ProfilCard from '../profilcard/profilcard';

const ProfilCardList = (props) => {
    const { data: dataR, error: errorR, loading: loadingR } = useQuery(getProfilsImages);
    if (loadingR) {
        return "loading..."

    }
    if (errorR) {
        return null;
    };
    return (
        <>
            {dataR.getProfilsImages.map((image) => {
                return (
                    <ProfilCard key={image.id} onClick={() => {props.handler(image.url) }} url={image.url}>{image.url}</ProfilCard>
                )
            })}
        </>
    );
};

export default ProfilCardList;