import React, { useEffect, useState } from 'react';
import styles from './Sidebar.module.scss';
import { AnimatePresence, motion, useCycle } from 'framer-motion';
import Link from 'next/link';


const Path = props => (
    <motion.path

        whileHover={{ scale: 1.1 }}
        fill="#e50914"
        strokeWidth="3"
        color="#e50914"
        stroke="hsl(0, 100%, 50%)"
        strokeLinecap="round"
        {...props}
    />
);
const sidebar = {
    open: (height = 1000) => ({
        clipPath: `circle(${height * 2 + 100}px at 0px 0px)`,
        transition: {
            type: "spring",
            stiffness: 10,
            restDelta: 2,
        }
    }),
    closed: {
        clipPath: "circle(30px at 40px 40px)",
        transition: {
            delay: 0.4,
            type: "spring",
            stiffness: 400,
            damping: 40,
        },

    }
};
const variants = {
    open: {
        transition: { staggerChildren: 0.2, delayChildren: 0.6 },
        zIndex: 8000
    },
    closed: {
        transition: { staggerChildren: 0.10, staggerDirection: -1 },
        zIndex: -8000
    }
};

const variantsItem = {
    open: {
        zIndex: 8000,
        y: 0,
        opacity: 1,
        transition: {
            y: { stiffness: 1000, velocity: -100 }
        }
    },
    closed: {
        y: 50,
        opacity: 0,
        zIndex: -8000,
        transition: {
            y: { stiffness: 1000 }
        }
    }
};


const Sidebar = (props) => {

    const [isOpen, toggleOpen] = useCycle(false, true);
    const [token, SetToken] = useState(false);
    const [userName, SetUserName] = useState();
    useEffect(() => {
        localStorage.getItem("token") ? SetToken(true) : SetToken(false)
        SetUserName(localStorage.getItem("userName"));

    }, [token])

    return (
        <AnimatePresence>
            <motion.nav className={isOpen ? `${styles.sidebar} ${styles.open}` : `${styles.sidebar}`}
                initial={false}
                animate={isOpen ? "open" : "closed"}>
                <button className={styles.sidebar__button} onClick={() => toggleOpen()}>
                    <svg className={styles.test} width="23" height="23" viewBox="0 0 23 23">
                        <Path
                            variants={{
                                closed: { d: "M 2 2.5 L 20 2.5" },
                                open: { d: "M 3 16.5 L 17 2.5" }
                            }}
                        />
                        <Path
                            d="M 2 9.423 L 20 9.423"
                            variants={{
                                closed: { opacity: 1 },
                                open: { opacity: 0 }
                            }}
                            transition={{ duration: 0.2 }}
                        />
                        <Path
                            variants={{
                                closed: { d: "M 2 16.346 L 20 16.346" },
                                open: { d: "M 3 2.5 L 17 16.346" }
                            }}
                        />
                    </svg>
                </button>
                <Link href={"/"}>
                    <motion.ul className={styles.head__logo} variants={variants}>
                        <motion.li
                            className={styles.navitem}
                            variants={variantsItem}
                            whileHover={{ scale: 1.1, x: 20 }}
                            whileTap={{ scale: 0.95 }}
                        >
                            <div className={styles.navitem__icon}>
                                <svg className="logo-md" viewBox="0 0 148 90"><path d="M105.06233,14.2806261 L110.999156,30 C109.249227,29.7497422 107.500234,29.4366857 105.718437,29.1554972 L102.374168,20.4686475 L98.9371075,28.4375293 C97.2499766,28.1563408 95.5928391,28.061674 93.9057081,27.8432843 L99.9372012,14.0931671 L94.4680851,-5.68434189e-14 L99.5313525,-5.68434189e-14 L102.593495,7.87421502 L105.874965,-5.68434189e-14 L110.999156,-5.68434189e-14 L105.06233,14.2806261 Z M90.4686475,-5.68434189e-14 L85.8749649,-5.68434189e-14 L85.8749649,27.2499766 C87.3746368,27.3437061 88.9371075,27.4055675 90.4686475,27.5930265 L90.4686475,-5.68434189e-14 Z M81.9055207,26.93692 C77.7186241,26.6557316 73.5307901,26.4064111 69.250164,26.3117443 L69.250164,-5.68434189e-14 L73.9366389,-5.68434189e-14 L73.9366389,21.8745899 C76.6248008,21.9373887 79.3120255,22.1557784 81.9055207,22.2804387 L81.9055207,26.93692 Z M64.2496954,10.6561065 L64.2496954,15.3435186 L57.8442216,15.3435186 L57.8442216,25.9996251 L53.2186709,25.9996251 L53.2186709,-5.68434189e-14 L66.3436123,-5.68434189e-14 L66.3436123,4.68741213 L57.8442216,4.68741213 L57.8442216,10.6561065 L64.2496954,10.6561065 Z M45.3435186,4.68741213 L45.3435186,26.2498828 C43.7810479,26.2498828 42.1876465,26.2498828 40.6561065,26.3117443 L40.6561065,4.68741213 L35.8121661,4.68741213 L35.8121661,-5.68434189e-14 L50.2183897,-5.68434189e-14 L50.2183897,4.68741213 L45.3435186,4.68741213 Z M30.749836,15.5928391 C28.687787,15.5928391 26.2498828,15.5928391 24.4999531,15.6875059 L24.4999531,22.6562939 C27.2499766,22.4678976 30,22.2495079 32.7809542,22.1557784 L32.7809542,26.6557316 L19.812541,27.6876933 L19.812541,-5.68434189e-14 L32.7809542,-5.68434189e-14 L32.7809542,4.68741213 L24.4999531,4.68741213 L24.4999531,10.9991564 C26.3126816,10.9991564 29.0936358,10.9054269 30.749836,10.9054269 L30.749836,15.5928391 Z M4.78114163,12.9684132 L4.78114163,29.3429562 C3.09401069,29.5313525 1.59340144,29.7497422 0,30 L0,-5.68434189e-14 L4.4690224,-5.68434189e-14 L10.562377,17.0315868 L10.562377,-5.68434189e-14 L15.2497891,-5.68434189e-14 L15.2497891,28.061674 C13.5935889,28.3437998 11.906458,28.4375293 10.1246602,28.6868498 L4.78114163,12.9684132 Z" id="Fill-14"></path></svg>
                            </div>
                        </motion.li>
                    </motion.ul>

                </Link>

                <motion.ul className={isOpen ? `${styles.navitems}` : `${styles.navitems} ${styles.visible}`} variants={variants}>
                    {
                        props.nav.map((item) => {
                            return (
                                <Link key={item.name}href="/">

                                    <motion.li
                                        id={item.name}
                                        className={styles.navitem}
                                        variants={variantsItem}
                                        whileHover={{ scale: 1.1, x: 20 }}
                                        whileTap={{ scale: 0.95 }}
                                    >
                                        <div className={styles.navitem__icon}>
                                            <svg viewBox="0 0 24 24">
                                                <path fill="currentColor" d={item.svg} />
                                            </svg>
                                        </div>
                                        <div className={styles.navitem__text}>{item.name}</div>
                                    </motion.li>
                                </Link>
                            )
                        })

                    }
                </motion.ul>
                <motion.ul className={isOpen ? `${styles.sidebar__profil}` : `${styles.sidebar__profil} ${styles.visible}`} variants={variants}>
                    {token ?
                        <Link href="/profil">
                            <motion.li
                                className={styles.navitem}
                                variants={variantsItem}
                                whileHover={{ scale: 1.1, x: 20 }}
                                whileTap={{ scale: 0.95 }}
                            >
                                <div className={styles.navitem__icon}>
                                    <svg viewBox="0 0 24 24">
                                        <path d="M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M7.07,18.28C7.5,17.38 10.12,16.5 12,16.5C13.88,16.5 16.5,17.38 16.93,18.28C15.57,19.36 13.86,20 12,20C10.14,20 8.43,19.36 7.07,18.28M18.36,16.83C16.93,15.09 13.46,14.5 12,14.5C10.54,14.5 7.07,15.09 5.64,16.83C4.62,15.5 4,13.82 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,13.82 19.38,15.5 18.36,16.83M12,6C10.06,6 8.5,7.56 8.5,9.5C8.5,11.44 10.06,13 12,13C13.94,13 15.5,11.44 15.5,9.5C15.5,7.56 13.94,6 12,6M12,11A1.5,1.5 0 0,1 10.5,9.5A1.5,1.5 0 0,1 12,8A1.5,1.5 0 0,1 13.5,9.5A1.5,1.5 0 0,1 12,11Z" />
                                    </svg>
                                </div>
                                <div className={styles.navitem__text}>{userName} | Profil</div>
                            </motion.li>
                        </Link>
                        :
                        <Link href="/login">
                            <motion.li
                                className={styles.navitem}
                                variants={variantsItem}
                                whileHover={{ scale: 1.1, x: 20 }}
                                whileTap={{ scale: 0.95 }}
                            >
                                <div className={styles.navitem__icon}>
                                    <svg viewBox="0 0 24 24">
                                        <path d="M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M7.07,18.28C7.5,17.38 10.12,16.5 12,16.5C13.88,16.5 16.5,17.38 16.93,18.28C15.57,19.36 13.86,20 12,20C10.14,20 8.43,19.36 7.07,18.28M18.36,16.83C16.93,15.09 13.46,14.5 12,14.5C10.54,14.5 7.07,15.09 5.64,16.83C4.62,15.5 4,13.82 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,13.82 19.38,15.5 18.36,16.83M12,6C10.06,6 8.5,7.56 8.5,9.5C8.5,11.44 10.06,13 12,13C13.94,13 15.5,11.44 15.5,9.5C15.5,7.56 13.94,6 12,6M12,11A1.5,1.5 0 0,1 10.5,9.5A1.5,1.5 0 0,1 12,8A1.5,1.5 0 0,1 13.5,9.5A1.5,1.5 0 0,1 12,11Z" />
                                    </svg>
                                </div>
                                <div className={styles.navitem__text}>Connexion</div>
                            </motion.li>
                        </Link>
                    }
                </motion.ul>
                <motion.div className={styles.background} variants={sidebar} />
            </motion.nav>
        </AnimatePresence>
    );
};

export default Sidebar;