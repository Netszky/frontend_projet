import { useQuery } from '@apollo/client';
import Link from 'next/link';
import React, { useState } from 'react';
import { useEffect } from 'react';
import { getMovie, getMovieByCategory } from '../../../graphql/film';
import Similar from '../Similar/Similar';
import CardSlider from '../slider/CardSlider';
import styles from './CardDescription.module.scss';
const CardDescription = (props) => {
    const { data, error, loading } = useQuery(getMovie, {
        variables: { id: props.id }, onCompleted: (data) => {

        }
    });
    return (
        <div className={styles.hover__card__container}>
            {loading ? <h1>Loading</h1> :
                <div className={styles.hover__card}>
                    <svg onClick={() => { props.cancelHover() }} className={styles.cancel__button} viewBox="0 0 24 24">
                        <path d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z" />
                    </svg>
                    <div className={styles.img__container}>
                        <img alt='movieimage' src={data.getMovie.image}></img>
                        <div className={styles.modal__tool}>
                            <Link href={`/play?id=${data.getMovie.id}`}>
                                <svg className={styles.modal__button} viewBox="0 0 24 24">
                                    <path d="M10,16.5V7.5L16,12M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
                                </svg>
                            </Link>
                            {
                                props.page === "home" ?
                                    !props.isWishList ?
                                        <svg onClick={() => { props.addWishList(data.getMovie.id, data.getMovie.name) }} className={styles.modal__button} viewBox="0 0 24 24">
                                            <path d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />

                                        </svg> :
                                        <svg onClick={() => { props.deleteWishList(data.getMovie.id, data.getMovie.name) }} className={styles.modal__button} viewBox="0 0 24 24">
                                            <path d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z" />

                                        </svg>
                                    :
                                    <svg onClick={() => { props.deleteWishList(data.getMovie.id, data.getMovie.name) }} className={styles.modal__button} viewBox="0 0 24 24">
                                        <path d="M12,2C17.53,2 22,6.47 22,12C22,17.53 17.53,22 12,22C6.47,22 2,17.53 2,12C2,6.47 6.47,2 12,2M15.59,7L12,10.59L8.41,7L7,8.41L10.59,12L7,15.59L8.41,17L12,13.41L15.59,17L17,15.59L13.41,12L17,8.41L15.59,7Z" />

                                    </svg>
                            }
                        </div>
                    </div>
                    <div className={styles.preview__modal}>
                        <div className={styles.detail__modal__container}>
                            <div className={styles.detail__modal}>
                                <div className={styles.detail__modal__left}>
                                    <div className={styles.modal__info}>
                                        <span className={styles.recommanded}>Recommandé à {Math.floor(Math.random() * (100 - 40) + 40)}%</span> <p className={styles.duration}>{data.getMovie.duration}</p>
                                    </div>
                                    <p className={styles.synopsis}>
                                        {data.getMovie.description}
                                    </p>
                                </div>
                                <div className={styles.detail__modal__right}>
                                    <div className={styles.detail__motal__right__person}>
                                        <span className={styles.label}>Distribution : </span>

                                        {!loading && data.getMovie.actor.map((actors) => {
                                            return <span key={actors.id}>{actors.name} </span>
                                        })}
                                    </div>
                                    <div className={styles.detail__motal__right__genre}>
                                        <span className={styles.label}> Ce programme est : </span>
                                        {!loading && data.getMovie.categories.map((cat) => {
                                            return <span key={cat.id}>{cat.name} </span>
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h1 className={styles.label__title}>Titre Similaires</h1>
                        {
                            !loading && data.getMovie.categories.map((cat) => {
                                return (<Similar key={cat.id} movie={props.id} title={cat.name} category={cat.id}></Similar>)
                        })}

                    </div>
                </div>
            }
        </div>
    );
};

export default CardDescription;