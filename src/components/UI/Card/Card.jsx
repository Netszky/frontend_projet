import { useMutation, useQuery } from '@apollo/client';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import {ToastContainer } from 'react-toastify';
import { getProfil, updateWishlist } from '../../../graphql/queries/profil';
import CardDescription from '../CardDescription/CardDescription';
import styles from './card.module.scss';
import { errorToast, infoToast, successToast } from '../../../utils/utils';

const Card = (props) => {
    const [hover, setHover] = useState(false);
    const [updateWish] = useMutation(updateWishlist);
    const [isWishList, setIsWishList] = useState();
    const hoverFalse = () => {
        setHover(false)
    }
    const hoverTrue = (id) => {
        const wishlist = [];
        data.getProfil.wishlist.map((movie) => {
            wishlist.push(movie.id)
        })
        if(wishlist.includes(id)){
            setIsWishList(true);
        } else {
            setIsWishList(false);
        }
        setHover(true);
    }
    const [profil, setProfil] = useState(false);
    const { error, loading, data, refetch } = useQuery(getProfil, {
        variables: {
            id: profil.id
        }
    })
    const [addWishlist] = useMutation(updateWishlist);
    useEffect(() => {
        setProfil(JSON.parse(localStorage.getItem("profil")));
    }, [])

    const addWishlistHandler = (id, name) => {
        console.log(data.getProfil.wishlist)
        const wishlist = [];
        data.getProfil.wishlist.map((movie) => {
            wishlist.push(movie.id)
        })
        if (wishlist.includes(id)) {
            infoToast(name)
        } else {
            wishlist.push(id);
            addWishlist({
                variables: {
                    id: profil.id,
                    wishlist: wishlist
                }, onCompleted: (data) => {
                    successToast(name);
                    setIsWishList(true);
                    refetch();
                }
            })
        }
    }
    const deleteFromWishlist = (newWishlist, name) => {
        const wish = []
        data.getProfil.wishlist.map((movie) => [
            wish.push(movie.id)
        ])
        const indexofRemove = wish.indexOf(data.getProfil.wishlist)
        wish.splice(indexofRemove, 1);

        updateWish({
            variables: {
                id: profil.id,
                wishlist: wish
            }, onCompleted: (data) => {
                setIsWishList(false);
                errorToast(name);
                refetch();
            }
        }
        )
    }
    return (
        <>
        {hover && <CardDescription isWishList={isWishList} wishlist={data.getProfil.wishlist} page="home" cancelHover={hoverFalse} deleteWishList={deleteFromWishlist} addWishList={addWishlistHandler} id={props.movie.id}></CardDescription>}
        <ToastContainer/>
        <div className={styles.card}>
            <div className={styles.titlecard__container}>
                <img width="100%" height="100%" src={props.movie.image}></img>
            </div>
            <button onClick={() => {hoverTrue(props.movie.id), window.scrollTo(0,0)}} className={styles.arrow}>
                <svg viewBox="0 0 24 24">
                    <path fill="currentColor" d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />
                </svg>
            </button>
        </div>
        </>
    );
};

export default Card;


{/* <div className={!hover ? styles.card__image : styles.card__image__focus}>
                <img alt='movieimage' height="100%" width="100%" src={props.movie.image}></img>
                {hover && <Video url={props.movie.url}></Video>}
            </div>
            <div className={!hover ? styles.card__description : styles.card__description__focus}>
                <div className={styles.categories}>
                    {props.movie.categories.map((categorie) => {
                        return (
                            <p>{categorie.name}</p>
                        )
                    })}
                    <Link href={`/play?id=${props.movie.id}`}>
                        <svg className={styles.add__button} viewBox="0 0 24 24">
                            <path d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M10,16.5L16,12L10,7.5V16.5Z" />
                        </svg>
                    </Link>
                    <svg onClick={() => { addWishlistHandler(props.movie.id) }} className={styles.add__button} viewBox="0 0 24 24">
                        <path  d="M17,13H13V17H11V13H7V11H11V7H13V11H17M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3Z" />
                    </svg>
                </div>
                <div>
                    <p>{props.movie.duration}</p>
                </div>
            </div> */}