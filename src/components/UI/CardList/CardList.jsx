import React, { useState } from 'react';
import CardDescription from '../CardDescription/CardDescription';
import styles from './CardList.module.scss';

const CardList = (props) => {
    const [hover, setHover] = useState(false);
    const handleMouseEnter = async (id) => {
            setHover(true);
    };
    const handleMouseLeave = () => {
        setHover(false);
    };
    
    return (
        <div key={props.movie.id} className={hover ? styles.card__ : styles.card}>
            <svg onClick={() => { handleMouseEnter(props.movie.id) }} className={hover ? styles.show__modal__invisible : styles.show__modal} viewBox="0 0 24 24">
                <path fill="currentColor" d="M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4M7,10L12,15L17,10H7Z" />
            </svg>
            <img alt='movieimage' src={props.movie.image}></img>
            {hover && <CardDescription page="wishlist" id={props.movie.id} movie={props.movie.id} deleteWishList={props.onClick} cancelHover={handleMouseLeave}></CardDescription>}
        </div>
    );
};

export default CardList;