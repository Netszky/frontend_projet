import gql from "graphql-tag";

export const getUser = gql`
  query getUser($id: ID){
    getUser(id: $id){
      id
      firstname
      stripeID
      isSub
      sub{
        id
        type
        price
        create_date
      }
    }
  }
`
export const updateUser = gql `
    mutation updateUser($id: ID, $isSub: Boolean){
      updateUser(id: $id, isSub: $isSub){
        isSub
      }
    }
`
export const getUserProfiles = gql`
  query {
    getUserProfil{
        profil{id,name, image}
    }
  }
`;  

export const updateUserProfil = gql`
  mutation updateUser($id: ID,$profil: ID){
    updateUser(id: $id, profil: $profil){
      profil{name}
    }
  }
`