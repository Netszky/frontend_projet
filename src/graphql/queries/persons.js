import gql from "graphql-tag";

export const getPersons = gql `
    query{getPersons{id,name}}
`
export const createPerson = gql `
      mutation createPerson($name: String){
          createPerson(name: $name)
          {
              name
          }
      }
`