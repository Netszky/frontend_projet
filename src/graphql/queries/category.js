import gql from "graphql-tag";

export const getCategories = gql `
    query{getCategories{id,name}}
`
export const updateCategory = gql `
    mutation updateCategory($id: ID, $name:String){
        updateCategory(id:$id, name:$name){
            id,
            name
        }
    }
`
export const createCategory = gql `
    mutation createCategory($name: String){
        createCategory(name: $name){
            id,
            name
        }
    }
`