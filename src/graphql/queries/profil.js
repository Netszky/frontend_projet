import gql from "graphql-tag";


export const getProfil = gql`
    query getProfil($id: ID){
        getProfil(id: $id){
            wishlist{
             id,
             name,
             categories{name},
             actor{name},
             duration, 
             director{name}, 
             description, 
             year, 
             url,
             image
            }
        }
    }
`

export const updateProfil = gql`
    mutation updateProfil($id: ID, $name: String, $image: String){
        updateProfil(id: $id,name: $name, image: $image){
            name,
            image,
        }
    }
`
export const updateWishlist = gql `
    mutation updateWishlist($id: ID, $wishlist: [ID]){
        updateWishlist(id: $id, wishlist: $wishlist){
            wishlist{name}
        }
    }
`

export const deleteProfil = gql`
    mutation deleteProfil($id: ID){
        deleteProfil(id: $id){
            message
            code
        }
    }
`
export const createProfil = gql`
    mutation createProfil($name: String, $image: String){
         createProfil(name: $name,image: $image){
            id
            name
            image
        }
    }
`