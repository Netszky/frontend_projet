import gql from "graphql-tag";


export const getMovies = gql`
    query{getMovies{id,name,categories{name},actor{name},duration, director{name}, description, year, image, url}}
`
// export const getMoviesByCategories = gql`
//     query getMoviesByCategories($id: [String]){
//         getMoviesByCategories(categories: $id){
//             id,
//              name,
//              categories{name},
//              actor{name},
//              duration, 
//              director{name}, 
//              description, 
//              year, 
//              url,
//              image
//         }
//     }
// `
export const getMovieByCategory = gql`
    query getMoviesByCategory($id: String){
        getMoviesByCategory(categories: $id)
        {
            id,
             name,
             categories{id,name},
             actor{name},
             duration, 
             director{name}, 
             description, 
             year, 
             url,
             image
        }
    }
`
export const getMovie = gql`
     query getMovie($id: ID){
         getMovie(id: $id)
         {
             id,
             name,
             categories{id,name},
             actor{name},
             duration, 
             director{name}, 
             description, 
             year, 
             url,
             image
        }
    }
`
export const createMovie = gql`
    mutation createMovie($name: String, $categories: [ID], $actor: [ID], $duration: String, $director: [ID], $description: String, $year: Int, $image: String, $url: String){
        createMovie(Movie: {name: $name, categories: $categories, actor: $actor,duration: $duration, director: $director, description: $description, year: $year, image: $image, url:$url}){
            name
            categories{name},
            actor{name},
            duration,
            director{name},
            description,
            year,
            url,
            image
        }
    }
`
export const updateMovie = gql`
    mutation updateMovie($id: ID, $name: String, $categories: [ID], $actor: [ID], $duration: String, $director: [ID], $description: String, $year: Int, $image: String, $url: String){
        updateMovie(Movie: {id: $id, name: $name, categories: $categories, actor: $actor,duration: $duration, director: $director, description: $description, year: $year, image: $image, url:$url}){
            categories,
            actor,
            duration,
            director,
            description,
            year,
            url,
            image
        }
    }
`