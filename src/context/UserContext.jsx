import { createContext, useState, useEffect } from 'react';

const UserContext = createContext({
    token: typeof window !== "undefined" ? localStorage.getItem('token') : [],
    user: typeof window !== "undefined" ? JSON.parse(localStorage.getItem('user')) : [],
    setTokn: () => {}

})


export const UserContextProvider = ({ children }) => {
    const [token, setToken] = useState(typeof window !== "undefined" ? localStorage.getItem('token') : []);
    const [user, setUser] = useState(typeof window !== "undefined" ? JSON.parse(localStorage.getItem('user')) : []);
    const setTokn = (token) => {setToken(token)}

    const context = { token, user, setTokn};
    useEffect(() => {
        setToken(localStorage.getItem('token'));
        setUser(JSON.parse(localStorage.getItem("user")));
    }, [])

    return (
        <UserContext.Provider value={context}>
            {children}
        </UserContext.Provider>
    )
}
export default UserContext;