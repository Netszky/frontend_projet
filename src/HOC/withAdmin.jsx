import React, { useEffect, useState } from 'react';
import authService from '../services/auth.service';
import Router from 'next/router';


/**
 * Component for protected route
 * @param {*} WrappedComponent 
 * @returns 
 */
const withAdmin = (WrappedComponent) => {
    return (props) => {
        const [verify, setVerify] = useState(false);

        useEffect(() => {
            const token = localStorage.getItem("token");
                authService.verifyAdmin(token)
                    .then(
                        (data) => {
                            console.log(data);
                            if (data.verified) {
                                setVerify(true);
                            } else {
                                localStorage.removeItem("token");
                                Router.push("/login");
                            }
                        }
                    )
                    .catch((err) => {
                        localStorage.removeItem("token");
                        Router.push("/login")
                    }
                    )
        }, [])
        if (verify) {
            return (
                <WrappedComponent {...props} />
            )
        } else {
            return null;
        }
    }
};

export default withAdmin;