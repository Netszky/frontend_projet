import React, { useEffect, useState } from 'react';
import authService from '../services/auth.service';
import Router from 'next/router';


/**
 * Component for protected route
 * @param {*} WrappedComponent 
 * @returns 
 */
const withSub = (WrappedComponent) => {
    return (props) => {
        const [verify, setVerify] = useState(false);
        useEffect(() => {
            const token = localStorage.getItem("token");
            if (token) {
                authService.verifySub(token)
                    .then(
                        (data) => {
                            if (data.verified) {
                                setVerify(true);
                            } else {
                                Router.push("/checkout");
                            }
                        }
                    )
                    .catch((err) => {
                        Router.push("/checkout")
                    }
                    )
            } else {
                Router.push('/login')
            }
        }, [])
        if (verify) {
            return (
                <WrappedComponent {...props} />
            )
        } else {
            return null;
        }
    }
};

export default withSub;