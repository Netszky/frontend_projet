export default {

    updateUser(token, user){
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/users/update-user",{
            method: "PUT",
            headers: {
                "authorization": token,
                "content-type": "application/json",
            },
            body : JSON.stringify(user),
        })
        .then(res=>res.json())
    }
}