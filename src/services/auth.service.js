export default {
    
    register (user){
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/users/register",{
            method: "POST",
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*"
            },
            body : JSON.stringify(user)
        })
        .then(res=>res.json())
    },
    login (user){
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/users/login",{
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body : JSON.stringify(user)
        })
        .then(res=>res.json())
    },
    getUser(token){
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/users/get-user", {method: "GET", headers:{
            "authorization": token
        },})
        .then(res => res.json())
    },
    verifyToken(token){
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/users/verify-token", {method:"GET", headers:{
            "authorization": token
        },}).then(res => res.json());
    },
    verifySub(token){
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/users/verify-sub", {method:"GET", headers:{
            "authorization": token
        },}).then(res => res.json());
    },
    verifyAdmin(token){
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/users/verify-admin", {method:"GET", headers:{
            "authorization": token
        },}).then(res => res.json());
    }
}