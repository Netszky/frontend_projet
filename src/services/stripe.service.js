export default {
    createSession(token,body){
        console.log(body)
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/checkout/",{
            method: "POST",
            headers: {
                "content-type": "application/json",
                "authorization": token
            },
            body: JSON.stringify(body),
        })
        .then((res)=>res.json())
    },
    cancelSub(token, body){
        console.log(body)
        return fetch(process.env.NEXT_PUBLIC_API_URL+"api/v1/checkout/cancel", {
            method: "POST",
            headers: {
                "content-type": "application/json",
                "authorization": token
            },
            body: JSON.stringify(body)
        })
    }
}