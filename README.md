# CE PROJET

    Application Netflix Front faite avec Next.js.
## Pourquoi ces Technologies
    Docker : Conteneurisation de l'application. Hébergement du conteneur sur le docker hub.
        - Voir dockerfile pour la création de l'image de l'application
    Kubernetes : Orchestration de docker. Plus sécurisé que Docker Compose (car mode admin directement).
        - Voir k8snetflix.yml pour le fichier de déploiement Kubernetes


## Installation du projet
### Avec Node 
- git clone 
- cd frontend_projet
- npm install
- npm run dev
-> localhost:3000

### Avec Kubernetes 
#### Installation HELM et nginx ingress controller
- choco install helm

- helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

- helm install nginx-ingress ingress-nginx/ingress-nginx --namespace my-ingress --create-namespace --set controller.replicaCount=2 --set controller.nodeSelector."kubernetes\.io/os"=linux 

#### Verification
- kubectl get ns Vous devriez voir "my-ingress"

- kubectl apply -f k8snetflix.yml

- kubectl get pod -n my-ingress Normalent 2 POD running

- localhost:80



