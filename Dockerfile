FROM node:16.10.0

ARG NEXT_PUBLIC_STRIPE_KEY=pk_test_51KHUEnJslrGuoRGTo12dGxQQNkv7aZ8rDuTNZVpEfA8ADkafRIY67rg7rf7PJZB1wjMns5HeVbvHK0bMejYPWmPh00ONTA7VeO

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Installing dependencies
COPY package*.json /usr/src/app/
RUN npm install

# Copying source files
COPY . /usr/src/app

# Building app
RUN npm run build
EXPOSE 3000

# Running the app
CMD "npm" "run" "start"