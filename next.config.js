const { redirect } = require("next/dist/server/api-utils")

module.exports = {
  reactStrictMode: true,
  env: {
    NEXT_PUBLIC_API_URL:process.env.NEXT_PUBLIC_API_URL,
    NEXT_PUBLIC_STRIPE_KEY:process.env.NEXT_PUBLIC_STRIPE_KEY
  }
}
module.exports = {
  async redirect() {
    return [
      {
        source: '/',
        destination: '/home',
        permanent: true
      }
    ]
  }
}